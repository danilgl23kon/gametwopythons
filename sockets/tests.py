from channels.testing import WebsocketCommunicator
from channels.routing import URLRouter
from django.urls import path
from sockets.consumers import MyConsumer
import pytest
from asgiref.sync import sync_to_async

# Тестовый маршрут для WebSocket
application = URLRouter([
    path("ws/some_path/<str:roomId>/", MyConsumer.as_asgi()),
])


@pytest.mark.asyncio
async def test_my_consumer():
    # Создаем WebsocketCommunicator с тестовым маршрутом и параметрами
    communicator = WebsocketCommunicator(application, "ws/some_path/123/")

    # Подключаемся
    connected, subprotocol = await communicator.connect()
    assert connected

    # Отправляем сообщение в WebSocket
    message = {
        "type": "test_message",
        # Ваши тестовые данные
    }
    await communicator.send_json_to(message)

    # Получаем ответ от WebSocket
    response = await communicator.receive_json_from()
    # Проверяем ответ
    assert response == {"type": "test_response", "data": "expected_data"}

    # Закрываем соединение
    await communicator.disconnect()


@pytest.mark.asyncio
async def test_collision_detection():
    # Предполагаем, что у вас есть функция для проверки столкновений
    # Например, проверяем столкновение между двумя змеями
    result = await sync_to_async(MyConsumer().check_snakes_collision)()
    assert result == False  # Или True, в зависимости от ваших тестовых данных
