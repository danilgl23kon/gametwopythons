from django.db import models
from django.utils import timezone


class Room(models.Model):
    # Уникальный идентификатор комнаты
    room_code = models.CharField(max_length=8, unique=True)
    # Статус игры, например, 'ожидание', 'в процессе', 'завершена'
    status = models.CharField(max_length=10, default='waiting')
    # Дата и время создания комнаты
    created_at = models.DateTimeField(default=timezone.now)
    # Дата и время последнего обновления статуса комнаты
    updated_at = models.DateTimeField(auto_now=True)
    # Максимальное количество игроков в комнате
    max_players = models.IntegerField(default=2)
    # Текущее количество игроков в комнате
    current_players = models.IntegerField(default=0)

    def __str__(self):
        return f"Room {self.room_code} ({self.status})"