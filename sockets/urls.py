from django.urls import path
from sockets.views import *
from django.urls import path, re_path
from sockets.consumers import *


app_name = 'SocketsConfig'

urlpatterns = [
    path('game/', get_game),
    path('menu/', get_menu),
]

websocket_urlpatterns = [
    path('ws/some_path/<str:roomId>/', MyConsumer.as_asgi()),
    re_path(r'ws/some_path/(?P<game_id>\w+)/$', MyConsumer.as_asgi()),
    path('ws/some_path/', MyConsumer.as_asgi()),
]