from django.shortcuts import render


def get_game(request):
    return render(request, "game/index.html")


def get_menu(request):
    return render(request, "game/menu.html")