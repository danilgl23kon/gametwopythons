import json
import random
from math import sqrt
from channels.generic.websocket import AsyncWebsocketConsumer
from datetime import datetime


class MyConsumer(AsyncWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        self.room_group_name = None
        self.room_code = None
        self.js = {}
        self.timestamp_start = datetime.now().timestamp()
        self.statistics = {'snakeOne': {"is_win": False, "count_eaten_fruits": 0},
                           'snakeTwo': {"is_win": False, "count_eaten_fruits": 0},
                           "timestamp_start": self.timestamp_start,
                           "timestamp_end": 0.0}
        self.width_pole = 580
        self.high_pole = 400

    # Получения финальной статистики
    def get_final_statistics(self):
        self.statistics["timestamp_end"] = datetime.now().timestamp()
        self.statistics["snakeOne"]["count_eaten_fruits"] = self.js["snakeOne"]["count_eaten_fruits"]
        self.statistics["snakeTwo"]["count_eaten_fruits"] = self.js["snakeTwo"]["count_eaten_fruits"]
        return self.statistics

    # Функция для проверки пересечения прямоугольников
    def check_collision(self, snake_x, snake_y, fruit_x, fruit_y, width=20, height=20):
        # Проверяем перекрытие по оси X
        overlap_x = (snake_x < fruit_x + width) and (snake_x + width > fruit_x)
        # Проверяем перекрытие по оси Y
        overlap_y = (snake_y < fruit_y + height) and (snake_y + height > fruit_y)
        return overlap_x and overlap_y

    # Функция для проверки пересечения змей
    def check_snakes_collision(self):
        # Перебор всех сегментов первой змеи
        for segment_one in self.js['snakeOne']['segments']:
            # Перебор всех сегментов второй змеи
            for segment_two in self.js['snakeTwo']['segments']:
                if self.check_collision(segment_one['x'], segment_one['y'], segment_two['x'], segment_two['y']):
                    # Если нашлось столкновение, возвращаем True
                    return True
        # Если столкновений не найдено, возвращаем False
        return False

    # Функция для проверки выход за границы
    def check_borders(self,):
        for segment_one in self.js['snakeOne']['segments']:
            # Перебор всех сегментов второй змеи
            for segment_two in self.js['snakeTwo']['segments']:
                if segment_one['x'] < 0 or segment_one['y'] < 0 or segment_two['x'] < 0 or segment_two['y'] < 0:
                    # Если вышел за границы, возвращаем True
                    return True
                if segment_one['y'] > 380 or segment_two['y'] > 380:
                    return True
                if segment_one['x'] > self.width_pole:
                    return True
                # Если не вышел за границы, возвращаем False
                return False

    async def connect(self):
        self.room_code = self.scope['url_route']['kwargs']['roomId']
        self.room_group_name = f'game_{self.room_code}'


        # Присоединяемся к группе комнаты
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):

        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'game_message',
                'message': {"type": "game_over",
                            "data": self.get_final_statistics()}
            }
        )

        print("gere")

        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        # Распаковка JSON-строки
        self.js = json.loads(text_data)
        # print(self.js)

        if "type" in self.js:
            return

        # Обработка пересечения двух змей
        if self.check_snakes_collision():
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'game_message',
                    'message': {"type": "game_over",
                                "data": self.get_final_statistics()}
                }
            )
            # await self.close()
            return

        # Обработка выход за рамки
        if self.check_borders():
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'game_message',
                    'message': {"type": "game_over",
                                "data": self.get_final_statistics()}
                }
            )
            # await self.close()
            return

        # Обработка движения для snakeTwo (бота)
        if self.js['snakeTwo']['is_bot']:
            self.update_bot_position_snake_two()

        # Обработка движения для snakeOne (бота)
        if self.js['snakeOne']['is_bot']:
            self.update_bot_position_snake_one()

        # Проверяем направление движения для змеи snakeOne
        if self.js['snakeOne']['is_up'] and (self.js['idSend'] == 1 or self.js['idSend'] is None):
            self.js['snakeOne']['segments'][0]['y'] -= self.js['snakeOne']['speed']
        elif self.js['snakeOne']['is_down']:
            self.js['snakeOne']['segments'][0]['y'] += self.js['snakeOne']['speed']
        elif self.js['snakeOne']['is_right']:
            self.js['snakeOne']['segments'][0]['x'] += self.js['snakeOne']['speed']
        elif self.js['snakeOne']['is_left']:
            self.js['snakeOne']['segments'][0]['x'] -= self.js['snakeOne']['speed']

        # Проверяем направление движения для змеи snakeTwo
        if self.js['snakeTwo']['is_up'] and (self.js['idSend'] == 2 or self.js['idSend'] is None):
            self.js['snakeTwo']['segments'][0]['y'] -= self.js['snakeTwo']['speed']
        elif self.js['snakeTwo']['is_down']:
            self.js['snakeTwo']['segments'][0]['y'] += self.js['snakeTwo']['speed']
        elif self.js['snakeTwo']['is_right']:
            self.js['snakeTwo']['segments'][0]['x'] += self.js['snakeTwo']['speed']
        elif self.js['snakeTwo']['is_left']:
            self.js['snakeTwo']['segments'][0]['x'] -= self.js['snakeTwo']['speed']

        # Проверяем, съела ли змея фрукт на каком-либо шаге
        for pos in self.js:
            if pos == 'fruits' or pos == 'idSend':
                continue
            snake_x, snake_y = self.js[pos]['segments'][0]['x'], self.js[pos]['segments'][0]['y']
            count_fruit = 0
            for fruit in self.js['fruits']:
                fruit_x, fruit_y = fruit['x'], fruit['y']
                # Если расстояние между змеей и фруктом меньше порога, считаем, что фрукт съеден
                if self.check_collision(snake_x, snake_y, fruit_x, fruit_y):
                    eater = pos
                    # Отправка сообщения на клиент с информацией о том, кто съел фрукт
                    self.js[eater]['count_eaten_fruits'] += 1
                    self.js['fruits'][count_fruit]["x"] = random.randint(0, self.width_pole)
                    self.js['fruits'][count_fruit]["y"] = random.randint(0, self.high_pole)
                    # Отправка сообщения всем в комнате
                    await self.channel_layer.group_send(
                        self.room_group_name,
                        {
                            'type': 'game_message',
                            'message': self.js
                        }
                    )
                count_fruit += 1
        # Отправка сообщения всем в комнате
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'game_message',
                'message': self.js
            }
        )

    # Обработчик для сообщений типа game_message
    async def game_message(self, event):
        message = event['message']

        # Отправка сообщения обратно в WebSocket
        await self.send(text_data=json.dumps(message))

    def update_bot_position_snake_two(self):
        # Находим ближайший фрукт
        closest_fruit = min(self.js['fruits'], key=lambda fruit: sqrt(
            (fruit['x'] - self.js['snakeTwo']['segments'][0]['x']) ** 2 + (
                        fruit['y'] - self.js['snakeTwo']['segments'][0]['y']) ** 2))

        # Определяем направление движения бота к фрукту
        if self.js['snakeTwo']['segments'][0]['x'] < closest_fruit['x']:
            self.js['snakeTwo']['segments'][0]['x'] += self.js['snakeTwo']['speed']
        elif self.js['snakeTwo']['segments'][0]['x'] > closest_fruit['x']:
            self.js['snakeTwo']['segments'][0]['x'] -= self.js['snakeTwo']['speed']

        if self.js['snakeTwo']['segments'][0]['y'] < closest_fruit['y']:
            self.js['snakeTwo']['segments'][0]['y'] += self.js['snakeTwo']['speed']
        elif self.js['snakeTwo']['segments'][0]['y'] > closest_fruit['y']:
            self.js['snakeTwo']['segments'][0]['y'] -= self.js['snakeTwo']['speed']

    def update_bot_position_snake_one(self):
        # Находим ближайший фрукт
        closest_fruit = min(self.js['fruits'], key=lambda fruit: sqrt(
            (fruit['x'] - self.js['snakeOne']['segments'][0]['x']) ** 2 + (
                        fruit['y'] - self.js['snakeOne']['segments'][0]['y']) ** 2))

        # Определяем направление движения бота к фрукту
        if self.js['snakeOne']['segments'][0]['x'] < closest_fruit['x']:
            self.js['snakeOne']['segments'][0]['x'] += self.js['snakeOne']['speed']
        elif self.js['snakeOne']['segments'][0]['x'] > closest_fruit['x']:
            self.js['snakeOne']['segments'][0]['x'] -= self.js['snakeOne']['speed']

        if self.js['snakeOne']['segments'][0]['y'] < closest_fruit['y']:
            self.js['snakeOne']['segments'][0]['y'] += self.js['snakeOne']['speed']
        elif self.js['snakeOne']['segments'][0]['y'] > closest_fruit['y']:
            self.js['snakeOne']['segments'][0]['y'] -= self.js['snakeOne']['speed']