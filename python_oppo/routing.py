from django.urls import re_path, path
from sockets import consumers

websocket_urlpatterns = [
    path('ws/some_path/<str:roomId>/', consumers.MyConsumer.as_asgi()),
]